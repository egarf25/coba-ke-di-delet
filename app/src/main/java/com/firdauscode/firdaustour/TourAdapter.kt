package com.firdauscode.firdaustour

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_item_view.view.*

class TourAdapter(var items: ArrayList<Tours>, var clickListener: OnTourItemClickListener) :
    RecyclerView.Adapter<TourViewHolder>() {
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TourViewHolder {
        lateinit var tourViewHolder: TourViewHolder
        tourViewHolder =
            TourViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_item_view, parent, false))
        return tourViewHolder
    }

    override fun onBindViewHolder(holder: TourViewHolder, position: Int) {
//        holder.tourName?.text=items.get(position).name
//        holder.tourDesc?.text=items.get(position).description
//        holder.tourPic.setImageResource(items.get(position).photo)

        /*  val tour = items[position]
          Glide.with(holder.itemView.context)
              .load(tour.photo)
              .apply(RequestOptions().override(55, 55))
              .into(holder.tourPic)*/

        /*holder.tourName.text = tour.name
        holder.tourDesc.text = tour.description*/

        holder.initialize(items.get(position), clickListener)

    }

}

class TourViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var tourName = itemView.tourname
    var tourDesc = itemView.tourdesc
    var tourPic = itemView.tourpic

    fun initialize(item: Tours, action: OnTourItemClickListener) {
        tourName.text = item.name
        tourDesc.text = item.description
        tourPic.setImageResource(item.photo)

        itemView.setOnClickListener {
            action.onItemClick(item, adapterPosition)
        }
    }
}

interface OnTourItemClickListener {
    fun onItemClick(item: Tours, position: Int)
}