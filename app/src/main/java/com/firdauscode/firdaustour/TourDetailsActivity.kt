package com.firdauscode.firdaustour

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.firdauscode.firdaustour.databinding.ActivityTourDetailsBinding
import kotlinx.android.synthetic.main.activity_tour_details.*

class TourDetailsActivity : AppCompatActivity() {
    lateinit var binding: ActivityTourDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tour_details)

        val actionBar = supportActionBar
        actionBar!!.title = "Details"
        actionBar.setDisplayHomeAsUpEnabled(true)

        tour_name.text = getIntent().getStringExtra("TOURNAME")
        tour_description.text = getIntent().getStringExtra("TOURDESC")
        tour_pic_detail.setImageResource(getIntent().getIntExtra("TOURPHOTO", 0))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}




