package com.firdauscode.firdaustour

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.firdauscode.firdaustour.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnTourItemClickListener {

    lateinit var binding: ActivityMainBinding
    lateinit var tourlist: ArrayList<Tours>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        tourlist = ArrayList()
        addTours()

        tourRecycler.layoutManager = LinearLayoutManager(this)
        tourRecycler.addItemDecoration(DividerItemDecoration(this, 1))
        tourRecycler.adapter = TourAdapter(tourlist, this)
    }

    fun addTours() {
        tourlist.add(
            Tours(
                "Batu Luhur",
                "Batu Luhur berada di kawasan Taman Nasional Gunung Ciremai. Di sini kamu bisa sekadar bersantai atau mencicipi wahana bermain yang menguji nyali, seperti flying fox dan jembatan dari seutas tali."
                , R.drawable.batu_luhur
            )
        )
        tourlist.add(
            Tours(
                "Bukit Panembongan",
                "Bukit Panembongan disebut-sebut mirip Kalibiru yang berada di Yogyakarta. Bukit ini menawarkan udara segar dengan panorama alam dari ketinggian yang sangat memanjakan mata."
                , R.drawable.bukit_panembongan
            )
        )
        tourlist.add(
            Tours(
                "Cibulan",
                "Pusat akuatik energik yang menawarkan kolam renang dengan ikan besar, seluncuran air & air mancur."
                , R.drawable.cibulan
            )
        )
        tourlist.add(
            Tours(
                "Curug Landung",
                "Batu Luhur berada di kawasan Taman Nasional Gunung Ciremai. Di sini kamu bisa sekadar bersantai atau mencicipi wahana bermain yang menguji nyali, seperti flying fox dan jembatan dari seutas tali."
                , R.drawable.curug_landung
            )
        )
        tourlist.add(
            Tours(
                "Curug Putri",
                "Curug Putri merupakan sebuah air terjun yang terletak di kaki Gunung Ciremai. Air terjun ini menyuguhkan suasana yang masih asri dengan aliran air yang sangat menyegarkan."
                , R.drawable.curug_putri
            )
        )
        tourlist.add(
            Tours(
                "Gunung Ciremai",
                "salah satu tujuan favorit pecinta alam keindahan alamnya yang masih terjaga membuat taman nasional ini terlihat asri dan sangat menawan"
                , R.drawable.gunung_ciremai
            )
        )
        tourlist.add(
            Tours(
                "Hutan Setianegara",
                "Hutan Desa Setianegara merupakan spot yang tepat buat kamu yang ingin sejenak menghindar dari ramainya perkotaan. Kawasan ini punya hawa yang sejuk dan masih sangat asri. Bisa kemah juga di sini, lho."
                , R.drawable.hutan_setianegara
            )
        )
        tourlist.add(
            Tours(
                "Palutungan",
                "Wisata Alam Palutungan merupakan kawasan wisata yang punya beberapa spot memukau, seperti Curug Putri dan Curug Landung yang sudah disebutkan di atas. Kamu bisa melakukan berbagai aktivitas outdoor seperti camping, hikking, atau trekking."
                , R.drawable.palutungan
            )
        )
        tourlist.add(
            Tours(
                "Sangkan Resort",
                "Taman rekreasi ini memiliki seluncur air, wahana, dan area untuk paintball, serta penginapan dan tempat makan"
                , R.drawable.sangkan_resort
            )
        )
        tourlist.add(
            Tours(
                "Telaga Biru",
                "Sesuai namanya, Telaga Biru Cicerem menyajikan telaga dengan air yang jernih kebiruan. Kamu bisa menikmati suasana sekitar yang rimbun, memberi makan ikan, hingga main air sembari snorkeling."
                , R.drawable.telaga_biru
            )
        )
        tourlist.add(
            Tours(
                "Telaga Nilem",
                "Tak kalah eksotis, ada Telaga Nilem yang berlokasi di Desa Kaduela. Airnya sangat jernih. Saking jernihnya, kamu bisa melihat dasar telaga yang dipenuhi bebatuan dan lumut hijau. Main air, mandi, sembari menikmati kesegaran air yang masih bersih jadi aktivitas menyenangkan di sini"
                , R.drawable.telega_nilem
            )
        )
        tourlist.add(
            Tours(
                "Waduk Darma",
                "selain berfungsi sebagai penampungan air untuk pengairan dan perikanan, saat sudah menjadi sebuah tempat rekreasi"
                , R.drawable.wadukdarma
            )
        )
    }

    override fun onItemClick(item: Tours, position: Int) {
        // Toast.makeText(this, item.name, Toast.LENGTH_SHORT).show()

        val intent = Intent(this, TourDetailsActivity::class.java)
        intent.putExtra("TOURNAME", item.name)
        intent.putExtra("TOURDESC", item.description)
        intent.putExtra("TOURPHOTO", item.photo)
        startActivity(intent)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        setMode(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    private fun setMode(selectedMode: Int) {
        when (selectedMode) {
            R.id.action_about -> {
                val moveIntent = Intent(this@MainActivity, AboutActivity::class.java)
                startActivity(moveIntent)

            }
        }
    }


}
